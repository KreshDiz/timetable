#include "timetable.h"
#include "ui_timetable.h"

TimeTable::TimeTable(QWidget *parent) :  QWidget(parent), ui(new Ui::TimeTable)
{
    ui->setupUi(this);
    tableView= ui->tableView;
    model = new QStandardItemModel(5, 4, tableView);
    QStringList headers;
    headers<<"Subject"<<"Room"<<"Clock"<<"Day";
    model->setHorizontalHeaderLabels(headers);
    tableView->setModel(model);


}

TimeTable::~TimeTable()
{
    delete ui;
}
