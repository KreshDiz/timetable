#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>

namespace Ui {
class TimeTable;
}

class TimeTable : public QWidget
{
    Q_OBJECT

public:
    explicit TimeTable(QWidget *parent = 0);
    ~TimeTable();

private:
    Ui::TimeTable *ui;
    QTableView *tableView;
    QStandardItemModel *model;

};

#endif // TIMETABLE_H
