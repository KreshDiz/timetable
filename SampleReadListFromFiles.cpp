#include <iostream>
#include <fstream>
#include <string>

void printList(std::ifstream& fin, const std::string& fileName)
{
	fin.open(fileName);

	if (!fin.is_open())
	{
		std::cout << "Error open file: " << fileName << std::endl;
		return;
	}

	std::string temp;
	std::cout << fileName << ':' << std::endl;
	while (std::getline(fin, temp))		std::cout << temp << std::endl;
	std::cout << std::endl;
	fin.close();
}

int main()
{
	std::ifstream fin;
	std::string filePath = "Resources/Lists/";

	std::string fileNameSubjects = "ListOfSubjects.list";
	std::string fileNameAuditoriums = "ListOfAuditoriums.list";
	std::string fileHours = "ListOfHours.list";
	std::string fileWeekdays = "ListOfWeekdays.list";

	printList(fin, filePath + fileNameSubjects);
	printList(fin, filePath + fileNameAuditoriums);
	printList(fin, filePath + fileHours);
	printList(fin, filePath + fileWeekdays);

	system("PAUSE");
	return 0;
}
